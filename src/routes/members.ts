import {Request, Response} from "express";

import members = require('../db.json'); //load our local database file
export class Members { 
    
    public routes(app): void { //received the express instance from app.ts file         
        app.route('/members')
        .get((req: Request, res: Response) => {            
            res.status(200).send(members);
        })               
    }
}