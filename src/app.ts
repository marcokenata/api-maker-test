import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Members } from "./routes/members";

class App {

    public app: express.Application;
    public memberRoute : Members = new Members();

    constructor() {
        this.app = express(); //run the express instance and store in app
        this.config();
        this.memberRoute.routes(this.app);
    }

    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
    }

}

export default new App().app;