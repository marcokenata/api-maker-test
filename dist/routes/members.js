"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const members = require("../db.json"); //load our local database file
class Members {
    routes(app) {
        app.route('/members')
            .get((req, res) => {
            res.status(200).send(members);
        });
    }
}
exports.Members = Members;
