"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const members_1 = require("./routes/members");
class App {
    constructor() {
        this.memberRoute = new members_1.Members();
        this.app = express(); //run the express instance and store in app
        this.config();
        this.memberRoute.routes(this.app);
    }
    config() {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
    }
}
exports.default = new App().app;
